#include "../DataBaseParser.h"


int main(int argc, char* argv[]){

  if(argc != 2){
    printf("Wrong number of arguments. Usage:\n\t./lexer \'filename\'\n");
    return 1;
  }

  Lexer lexer(argv[1]);
  if(!lexer.lex()){
    printf("Lexer failed.\n");
    return 1;
  }

  DataBaseParser parser(lexer);
  if(!parser.parse()){
    printf("Parser failed.\n");
    return 1;
  }  

  if(parser.getDataBaseType() != "mysql"){
    printf("Database type not correct.\n");
    return 1;
  }

  if(parser.getDataBasePort() != 1099){
    printf("Database port not correct.\n");
    return 1;
  }

  return 0;
}
