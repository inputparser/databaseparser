#include "DataBaseParser.h"

DataBaseParser::DataBaseParser(Lexer &lex){
  lexer = &lex;

  DBtype = "";
  DBname = "";
  DBprefix = "";
  DBport = 0;
}

bool DataBaseParser::parse (){

  Lexer::iterator it = lexer->begin();
  for (; it != lexer->end(); it++){

    unsigned int token = lexer->getCurrentToken().first;

    if (lexer->getNextToken().first != token::COLON){
      printf("Syntax Error(%d): Expected \":\", Found %s\n", lexer->getCurrentLineNumber(), lexer->getNextToken().second.c_str());
      return false;
    }
    lexer->advanceTokenStream();
    lexer->advanceTokenStream();
    it++; it++;

    switch(token){
      case token::TYPE:
    	  if(lexer->getCurrentToken().first != token::IDENTIFIER){
    	  	printf("Syntax Error (%d) Expected an identifier, found %s\n", lexer->getCurrentLineNumber(), lexer->getCurrentToken().second.c_str());
    	  	return false;
    	  }
        DBtype = lexer->getCurrentToken().second;
        printf("%s type found, set to %s\n", token::TokenName[token], DBtype.c_str());
        break;
      case token::NAME:
    	  if(lexer->getCurrentToken().first != token::IDENTIFIER){
    	  	printf("Syntax Error (%d) Expected an identifier, found %s\n", lexer->getCurrentLineNumber(), lexer->getCurrentToken().second.c_str());
    	  	return false;
    	  }
        DBname = lexer->getCurrentToken().second;
        printf("%s type found, set to %s\n", token::TokenName[token], DBname.c_str());
        break;
      case token::TABLE_PREFIX:
    	  if(lexer->getCurrentToken().first != token::IDENTIFIER){
    	  	printf("Syntax Error (%d) Expected an identifier, found %s\n", lexer->getCurrentLineNumber(), lexer->getCurrentToken().second.c_str());
    	  	return false;
    	  }
        DBprefix = lexer->getCurrentToken().second;
        printf("%s type found, set to %s\n", token::TokenName[token], DBprefix.c_str());
        break;
      case token::PORT:
    	  if(lexer->getCurrentToken().first != token::INTEGER){
    	  	printf("Syntax Error (%d) Expected an integer, found %s\n", lexer->getCurrentLineNumber(), lexer->getCurrentToken().second.c_str());
    	  	return false;
    	  }
        DBport = atoi(lexer->getCurrentToken().second.c_str());
        printf("%s type found, set to %d\n", token::TokenName[token], DBport);
        break;
      default:
        printf("Syntax Error(%d): Found: %s\n", lexer->getCurrentLineNumber(), lexer->getCurrentToken().second.c_str());
        return false;
    }

    lexer->advanceTokenStream();
  }



  return true;
}

std::string DataBaseParser::getDataBaseType () const {
  return DBtype;
}

std::string DataBaseParser::getDataBaseName () const {
  return DBname;
}

std::string DataBaseParser::getDataBasePrefix () const {
  return DBprefix;
}

unsigned int DataBaseParser::getDataBasePort () const {
  return DBport;
}



