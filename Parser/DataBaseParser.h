#ifndef PARSER_H_1K4O
#define PARSER_H_1K4O

#include <stdlib.h>
#include "../Lexer/Lexer.h"
#include "../Lexer/tokens.h"

class DataBaseParser {

  public:
    DataBaseParser (Lexer&);

    bool parse();

    std::string getDataBaseType() const;
    std::string getDataBaseName() const;
    std::string getDataBasePrefix() const;
    unsigned int getDataBasePort() const;

  private:
    Lexer* lexer;
    std::string DBtype, DBname, DBprefix;
    unsigned int DBport;

};

#endif // PARSER_H_1K4O
