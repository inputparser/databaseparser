#include <iostream>
#include "../lex.yy.c"

int main (int argc, char* argv[]){

  if (argc != 2){
    std::cout << "Incorrect Usage:\n";
    return 1;
  }

  yyin = fopen(argv[1], "r");

  int ntoken, vtoken;
  ntoken = yylex();
  while (ntoken){

    if(yylex() != token::COLON){
      std::cout << "Syntax Error(" << lineNumber << "): Expected \":\", found " << yytext << std::endl;
      return 1;
    }

    vtoken = yylex();
    switch (ntoken){
      case token::TYPE:
        if (vtoken != token::IDENTIFIER){
          std::cout << "Syntax Error (" << lineNumber << ") Expected an identifier, found " << yytext << std::endl;
          return 1;
        }
        std::cout << token::TokenName[ntoken] << " type found, set to " << yytext << std::endl;
        break;
      case token::NAME:
        if (vtoken != token::IDENTIFIER){
          std::cout << "Syntax Error (" << lineNumber << ") Expected an identifier, found " << yytext << std::endl;
          return 1;
        }
        std::cout << token::TokenName[ntoken] << " type found, set to " << yytext << std::endl;
        break;
      case token::TABLE_PREFIX:
        if (vtoken != token::IDENTIFIER){
          std::cout << "Syntax Error (" << lineNumber << ") Expected an identifier, found " << yytext << std::endl;
          return 1;
        }
        std::cout << token::TokenName[ntoken] << " type found, set to " << yytext << std::endl;
        break;
      case token::PORT:
        if (vtoken != token::INTEGER){
          std::cout << "Syntax Error (" << lineNumber << ") Expected an integer, found " << yytext << std::endl;
          return 1;
        }
        std::cout << token::TokenName[ntoken] << " type found, set to " << yytext << std::endl;
        break;
      default:
        std::cout << "Syntax Error(" << lineNumber << "): Found " << yytext << std::endl;
    }
    ntoken = yylex();
  }



  yy_delete_buffer(YY_CURRENT_BUFFER);
  yy_init = 1;
  fclose(yyin);


  return 0;
}
