#ifndef LEXER_H_42E3
#define LEXER_H_42E3

#include <stdio.h>
#include <vector>
#include <string>
#include "FlexLexer.h"
#include <fstream>
#include "tokens.h"

extern int lineNumber;

class Lexer {

  public:
    Lexer(const char*);
    Lexer(const std::istream&);
    ~Lexer ();

    bool lex ();

    typedef std::vector<std::pair<int, std::string> >::iterator iterator;
    typedef std::vector<std::pair<int, std::string> >::const_iterator const_iterator;
    iterator begin() { return tokenStream.begin(); }
    const_iterator begin() const { return tokenStream.begin(); }
    iterator end() { return tokenStream.end(); }
    const_iterator end() const { return tokenStream.end(); }


    std::pair<int, std::string> getCurrentToken () const;
    std::pair<int, std::string> getNextToken () const;
    unsigned int getNumberTokens () const;
    unsigned int getCurrentLineNumber () const;

    void advanceTokenStream ();
    void resetTokenStream ();
    void printTokens() const;
 
  private:
    std::vector<std::pair<int, std::string> > tokenStream;
    std::vector<unsigned int> lineNumbers;
    unsigned int currentTokenIndex;
    unsigned int numberTokens;

    yyFlexLexer baseLexer;
    std::fstream inputFile;
    std::iostream* inputStream;

};

#endif // LEXER_H_42E3

