%{
#include "tokens.h"
int lineNumber = 1;
%}

%option c++
%option noyywrap

%%

"db_type"		return token::TYPE;
"db_name"		return token::NAME;
"db_table_prefix"	return token::TABLE_PREFIX;
"db_port"		return token::PORT;
:			return token::COLON;
[a-zA-Z][_a-zA-Z0-9]*	return token::IDENTIFIER;
[1-9][0-9]*		return token::INTEGER;
[\n]			lineNumber++;
[ \t]+			;
.                       {printf("Syntax Error(%d): Unidentified Character \"%s\".\n",lineNumber,yytext);
                         return token::ERROR;}


%%



