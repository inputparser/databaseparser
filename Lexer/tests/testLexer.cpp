#include "../Lexer.h"
#include <sstream>

int main (int argc, char* argv[]){

  if(argc != 2){
    printf("Wrong number of arguments. Usage:\n\t./lexer \'filename\'\n");
    return 1;
  }

  Lexer lexer(argv[1]);
  if(!lexer.lex()){
    printf("Lexer failed.\n");
    return 1;
  }

  // Check that we get the right number of tokens
  if (lexer.getNumberTokens () != 12){
    printf("Incorrect number of tokens found.\n");
    return 1;
  }

  // Check that iterator works
  Lexer::iterator it = lexer.begin();
  for (; it != lexer.end(); it++){
    std::pair<int, std::string> tokenPair = lexer.getCurrentToken();
    printf("Line: %d\tToken Id: %d\tToken Type: %-17s\tToken Value: %s\n",lexer.getCurrentLineNumber(), tokenPair.first, token::TokenName[tokenPair.first], tokenPair.second.c_str());
    lexer.advanceTokenStream();
  }

  // Check that line numbers are correct
  lexer.resetTokenStream ();
  if (lexer.getCurrentLineNumber() != 1){
    printf("Line number incorrect\n\tExpected: 1, Result: %d\n",lexer.getCurrentLineNumber());
    return 1;
  }
  lexer.advanceTokenStream();
  if (lexer.getCurrentLineNumber() != 1){
    printf("Line number incorrect\n\tExpected: 1, Result: %d\n",lexer.getCurrentLineNumber());
    return 1;
  }
  for (unsigned int i = 0; i < 6; i++)
    lexer.advanceTokenStream();
  if (lexer.getCurrentLineNumber() != 3){
    printf("Line number incorrect\n\tExpected: 3, Result: %d\n",lexer.getCurrentLineNumber());
    return 1;
  } 


  std::stringstream input;
  input << "db_type : mysql\n";
  input << "db_name : test\n";
  input << "db_table_prefix : test_\n";
  input << "db_port : 1099\n";

  Lexer lexer2(input);
  if(!lexer2.lex()){
  	printf("Second Lexer failed.\n");
  	return 1;
  }

  // Check that we get the right number of tokens
  if (lexer2.getNumberTokens () != 12){
    printf("Incorrect number of tokens found.\n");
    return 1;
  }


  return 0;
}
