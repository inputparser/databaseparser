//#define TYPE 1
//#define NAME 2
//#define TABLE_PREFIX 3
//#define PORT 4
//#define COLON 5
//#define IDENTIFIER 6
//#define INTEGER 7
#ifndef TOKEN_H_12A5
#define TOKEN_H_12A5

class token{

  public:
    enum TokenType {EMPTY,
                    ERROR,
                    COLON,
                    IDENTIFIER,
                    INTEGER,
                    TYPE,
                    NAME,
                    TABLE_PREFIX,
                    PORT};
    static const unsigned int nTokens = 9;
    static const char* TokenName[nTokens];
};
/*
const char* token::TokenName[nTokens] = {"",
                                         "Error",
                                         ":",
                                         "identifier",
                                         "integer",
                                         "db_type",
                                         "db_name",
                                         "db_table_prefix",
                                         "db_port"};
*/
#endif // TOKEN_H_12A5
