#include "Lexer.h"

Lexer::Lexer (const char* input){
  printf("Input File: %s\n",input);

  inputFile.open(input);

  inputStream = new std::iostream(inputFile.rdbuf());
  baseLexer = yyFlexLexer(inputStream);

  currentTokenIndex = 0;
  numberTokens = 0;
}

Lexer::Lexer (const std::istream& input){
  printf("Input File: %s\n","Using inputStream");

  input.rdbuf()->pubseekpos(0, std::ios_base::in);
  inputStream = new std::iostream(input.rdbuf());

  baseLexer = yyFlexLexer(inputStream);

  currentTokenIndex = 0;
  numberTokens = 0;
}

Lexer::~Lexer (){
	if(inputFile.is_open())
		inputFile.close();
	delete inputStream;
}

bool Lexer::lex (){

  int ntoken = baseLexer.yylex();
  std::pair<int, std::string> tokenPair;

  while(ntoken){
    if (ntoken == token::ERROR)
      return false;

    tokenPair.first = ntoken;
    tokenPair.second = baseLexer.YYText();
    tokenStream.push_back(tokenPair);
    lineNumbers.push_back(lineNumber);
    ntoken = baseLexer.yylex ();
  }
  printf("Number of Tokens: %lu\n",tokenStream.size());
  numberTokens = tokenStream.size();

  return true;
}

std::pair<int, std::string> Lexer::getCurrentToken () const {
  return tokenStream[currentTokenIndex];
}

std::pair<int, std::string> Lexer::getNextToken () const {
  return tokenStream[currentTokenIndex+1];
}

unsigned int Lexer::getNumberTokens () const {
  return numberTokens;
}

unsigned int Lexer::getCurrentLineNumber () const {
  return lineNumbers[currentTokenIndex];
}

void Lexer::advanceTokenStream (){
  if (currentTokenIndex < tokenStream.size())
    currentTokenIndex++;
}

void Lexer::printTokens () const{
  for (unsigned int i = 0; i < tokenStream.size(); i++)
    printf("Token Id: %d\tToken Value: %s\n",tokenStream[i].first,tokenStream[i].second.c_str());
}

void Lexer::resetTokenStream () {
  currentTokenIndex = 0;
}

